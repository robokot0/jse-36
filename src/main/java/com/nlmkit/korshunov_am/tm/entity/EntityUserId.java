package com.nlmkit.korshunov_am.tm.entity;

public interface EntityUserId {
    /**
     * Установить ид пользователя владельца проекта
     * @param userId ид пользователя владельца проекта
     */
    void setUserId(Long userId);

    /**
     * Получить ид пользователя владельца проекта
     * @return ид пользователя владельца проекта
     */
    Long getUserId();

}

package com.nlmkit.korshunov_am.tm.exceptions;

/**
 * Исключение не найден проект
 */
public class ProjectNotFoundException extends MessageException {
    /**
     * Конструктор не найден проект по параметру
     * @param name  имя параметра
     * @param value значение параметра
     */
    public ProjectNotFoundException(String name,String value) {
        super("Не найден проект по "+name+"="+value);
    }
    /**
     * Конструктор не найден проект пользователя по параметру
     * @param name имя параметра
     * @param userId ид пользователя
     * @param value значение параметра
     */
    public ProjectNotFoundException(String name,final Long userId,String value) {
        super("Не найден проект пользователя "+userId.toString()+" по "+name+"="+value);
    }
}

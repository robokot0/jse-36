package com.nlmkit.korshunov_am.tm.exceptions;

/**
 * Исключение неправильный аргумент
 */
public class WrongArgumentException extends MessageException {
    /**
     * Конструктор
     * @param message Сообщение об ошибке
     */
    public WrongArgumentException(String message) {
        super(message);
    }
}

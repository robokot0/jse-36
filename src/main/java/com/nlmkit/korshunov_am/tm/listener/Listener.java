package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.publisher.InputConsole;

public interface Listener {
    /**
     * Указать объект из котрого получать параметры комманд
     * @param inputConsole объект из котрого получать параметры комманд
     */
    void setConsole(InputConsole inputConsole);
    /**
     * Оповестить слушателя о новой комманде
     * @param command комманда
     * @return 0 не обработана комманда 1 обработана
     */
    int notify(String command);
}

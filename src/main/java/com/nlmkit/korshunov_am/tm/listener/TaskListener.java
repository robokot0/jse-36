package com.nlmkit.korshunov_am.tm.listener;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class TaskListener extends AbstractListener implements Listener {
    /**
     * Сервис задач
     */
    TaskService taskService;
    /**
     * Листенер пользователей
     */
    UserListener userListener;
    /**
     * конструктор для тестирования
     */
    TaskListener(TaskService taskService,ProjectTaskService projectTaskService,CommandHistoryService commandHistoryService,UserListener userListener){
        super(commandHistoryService);
        this.taskService=taskService;
        this.projectTaskService=projectTaskService;
        this.userListener=userListener;
    }

    /**
     * Приватный конструктор по умолчанию
     */
    private TaskListener(){
        super(CommandHistoryService.getInstance());
        this.taskService = TaskService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
        this.userListener=UserListener.getInstance();
    }
    /**
     * Единственный экземпляр объекта TaskListener
     */
    private static TaskListener instance = null;

    /**
     * Получить единственный экземпляр объекта TaskListener
     * @return единственный экземпляр объекта TaskListener
     */
    public static TaskListener getInstance(){
        if (instance == null){
            instance = new TaskListener();
        }
        return instance;
    }
    /**
     * Получить текущего пользователя
     * @return текущий пользователь
     */
    @Override
    public User getUser() {
        return userListener.getUser();
    }

    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_TASK_CREATE:
                case TASK_CREATE:return createTask();
                case SHORT_TASK_CLEAR:
                case TASK_CLEAR:return clearTask();
                case SHORT_TASK_LIST:
                case TASK_LIST:return listTask();
                case SHORT_TASK_VIEW:
                case TASK_VIEW:return viewTaskByIndex();
                case SHORT_TASK_REMOVE_BY_ID:
                case TASK_REMOVE_BY_ID:return removeTaskByID();
                case SHORT_TASK_REMOVE_BY_NAME:
                case TASK_REMOVE_BY_NAME:return removeTaskByName();
                case SHORT_TASK_REMOVE_BY_INDEX:
                case TASK_REMOVE_BY_INDEX:return removeTaskByIndex();
                case SHORT_TASK_UPDATE_BY_INDEX:
                case TASK_UPDATE_BY_INDEX:return updateTaskByIndex();
                case SHORT_TASK_ADD_TO_PROJECT_BY_IDS:
                case TASK_ADD_TO_PROJECT_BY_IDS:return addTaskToProjectByIds();
                case SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS:
                case TASK_REMOVE_FROM_PROJECT_BY_IDS:return removeTaskFromProjectByIds();
                case SHORT_TASK_LISTS_BY_PROJECT_ID:
                case TASK_LISTS_BY_PROJECT_ID:return listTaskByProjectId();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return saveAsJson();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return saveAsXML();
                case SHORT_LOAD_FROM_XML:
                case LOAD_FROM_XML: return loadFromXML();
                case SHORT_LOAD_FROM_JSON:
                case LOAD_FROM_JSON: return loadFromJSON();
                case SHORT_USER_OF_TASK_SET_BY_INDEX:
                case USER_OF_TASK_SET_BY_INDEX:return setTaskUserByIndex();
                case SHORT_HELP:
                case HELP: return displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            ShowResult("[FAIL] "+e.getMessage());
        }
        return 0;
    }

    /**
     * Сервис задач в проекте
     */
    private final ProjectTaskService projectTaskService;
    /**
     * Изменить задачу
     * @param task задача
     */
    public void updateTask(final Task task) throws WrongArgumentException {
        final String name = enterStringCommandParameter("task name");
        final String description = enterStringCommandParameter("task description");
        final LocalDateTime localDateTime = enterLocalDateTimeCommandParameter("deadline",false);
        taskService.update(task.getId(),name,description,task.getUserId(),localDateTime);
        ShowResult("[OK]");
    }
    /**
     * Изменить задачу по индексу
     * @return 0 выполнено
     */
    public int updateTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[UPDATE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("task index")-1;
        updateTask(this.getUser().isAdmin()
            ?taskService.findByIndex(index,true)
            :taskService.findByIndex(index,this.getUser().getId(),true));
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByName() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = enterStringCommandParameter("task name");
        if (this.getUser().isAdmin()) {
            taskService.removeByName(name);
        } else {
            taskService.removeByName(name, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по ID
     * @return 0 выполнено
     */
    public int removeTaskByID() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        if (!this.testAuthUser())return 0;
        final long id = enterLongCommandParameter("task ID");
        if(this.getUser().isAdmin()) {
            taskService.removeById(id);
        } else {
            taskService.removeById(id, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("task index")-1;
        if(this.getUser().isAdmin()) {
            taskService.removeByIndex(index);
        } else {
            taskService.removeByIndex(index, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Создать задачу
     * @return 0 выполнено
     */
    public int createTask() throws WrongArgumentException {
        System.out.println("[CREATE TASK]");
        if (!this.testAuthUser())return 0;
        final String name = enterStringCommandParameter("task name");
        final String description = enterStringCommandParameter("task description");
        final LocalDateTime localDateTime = enterLocalDateTimeCommandParameter("deadline (default +8 hours)",false);
        taskService.create(name,description,this.getUser().getId(), localDateTime==null?LocalDateTime.now().plusHours(8):localDateTime);
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все задачи
     * @return 0 выполнено
     */
    public int clearTask() throws WrongArgumentException {
        System.out.println("[CLEAR TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().isAdmin()) {
            taskService.clear();
        } else {
            taskService.clear(this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать информацию по задаче
     * @param task задача
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("DEADLINE: " + (task.getDeadLine()==null?"":task.getDeadLine().toString()) + "(" + (task.getDeadLine()==null?"":localDateTime.until(task.getDeadLine(), ChronoUnit.MINUTES)) + " min)");
        System.out.println("USER: " + (task.getUserId()==null?"":
                userListener.findById(task.getUserId())==null?"":userListener.findById(task.getUserId()).getLogin()));
        ShowResult("[OK]");
    }

    /**
     * Показать задачу по индексу
     * @return 0 выполнено
     */
    public int viewTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAuthUser())return 0;
        final int index = enterIntegerCommandParameter("task index")-1;
        viewTask(this.getUser().isAdmin()
            ?taskService.findByIndex(index,true)
            :taskService.findByIndex(index,this.getUser().getId(),true));
        return 0;
    }

    /**
     * Показать список задач
     * @param tasks список
     */
    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        LocalDateTime localDateTime = LocalDateTime.now();
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName() + ": " + (task.getDeadLine()==null?"":localDateTime.until(task.getDeadLine(), ChronoUnit.MINUTES)) );
            index ++;
        }
    }

    /**
     * Показать список задач
     * @return 0 выполнено
     */
    public int listTask() throws WrongArgumentException {
        System.out.println("[LIST TASK]");
        if (!this.testAuthUser())return 0;
        viewTasks(this.getUser().isAdmin()
            ?taskService.findAll()
            :taskService.findAll(getUser().getId()));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать список задач по идентфиикатору проекта
     * @return 0 выполнено
     */
    public int listTaskByProjectId() throws WrongArgumentException {
        System.out.println("[LIST TASK BY PROJECT ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = enterLongCommandParameter("project ID");
        if(this.getUser().isAdmin()) {
            viewTasks(taskService.findAllByProjectId(projectId));
        } else {
            viewTasks(taskService.findAllByProjectId(projectId, this.getUser().getId()));
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Добавить задачу в проект по идентфиикатору
     * @return 0 выполнео
     */
    public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException, WrongArgumentException {
        final long projectId = enterLongCommandParameter("project ID");
        final long taskId = enterLongCommandParameter("task ID");
        projectTaskService.addTaskToProject(projectId,taskId);
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу из проекта по идентификатору
     * @return 0 выполнено
     */
    public int removeTaskFromProjectByIds() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = enterLongCommandParameter("project ID");
        final long taskId = enterLongCommandParameter("task ID");
        if(this.getUser().isAdmin()) {
            projectTaskService.removeTaskFromProject(projectId, taskId);
        } else {
            projectTaskService.removeTaskFromProject(projectId, taskId, this.getUser().getId());
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @param user Пользователь на ид которого менять
     */
    public void setTaskUserById(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return;
        final long taskId = enterLongCommandParameter("task ID");
        final Task task = taskService.findById(taskId,true);
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId(),LocalDateTime.now().plusHours(5));
        ShowResult("[OK]");
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @param user Пользователь на ид которого менять
     */
    public void setTaskUserByIndex(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return;
        final int index = enterIntegerCommandParameter("task index")-1;
        final Task task = taskService.findByIndex(index,true);
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId(),LocalDateTime.now().plusHours(5));
        ShowResult("[OK]");
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("----Task commands:");
        logger.info("task-create - Create new task by name. (tcr)");
        logger.info("task-clear - Remove all tasks. (tcl)");
        logger.info("task-list - Display list of tasks. (tl)");
        logger.info("task-view - View task info. (tv)");
        logger.info("task-remove-by-id - Remove task by id. (trid)");
        logger.info("task-remove-by-name - Remove task by name. (trn)");
        logger.info("task-remove-by-index - Remove task by index. (trin)");
        logger.info("task-update-by-index - Update name and description of task by index. (tuin)");
        logger.info("----Task in project commands:");
        logger.info("task-list-by-project-id - Display task list by project if. (tlp)");
        logger.info("task-add-to-project-by-ids - Add task to project by id. (tap)");
        logger.info("task-remove-from-project-by-ids - Remove task from project by id. (trp)");
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsJson() throws IOException{
        taskService.saveAs(new JsonMapper(),new FileOutputStream(this.getClass().getName()+".json"));
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsXML() throws IOException {
        taskService.saveAs(new XmlMapper(),new FileOutputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    public int loadFromXML()  throws IOException {
        taskService.loadFrom(new XmlMapper(),new FileInputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    public int loadFromJSON()  throws IOException {
        taskService.loadFrom(new JsonMapper(),new FileInputStream(this.getClass().getName()+".json"));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @return 0 выполнено
     */
    public int setTaskUserByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[SET TASK USER FIND TASK BY INDEX]");
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final  User user = userListener.findByLogin(login);
        setTaskUserByIndex(user);
        return 0;
    }

}

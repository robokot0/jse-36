package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.TaskRemoveExpiredService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.USER_END;

public class TaskRemoveExpiredListener extends AbstractListener implements Listener {
    /**
     * Ссылка на listener ользователей
     */
    UserListener userListener;

    /**
     * Ссылка на Executor
     */
    ScheduledExecutorService executorService;
    /**
     * Ссылка на запуск по расписанию
     */
    ScheduledFuture scheduledFuture = null;
    /**
     * Сервис оповещений по задачам
     */
    TaskRemoveExpiredService taskRemoveExpiredService;
    /**
     * Приватный кконструктор по умолчанию
     */
    private TaskRemoveExpiredListener() {
        super(CommandHistoryService.getInstance());
        this.taskRemoveExpiredService = TaskRemoveExpiredService.getInstance();
        this.userListener = UserListener.getInstance();
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }
    /**
     * Конструктор длоя тестирвоания
     * @param taskRemoveExpiredService Сервис удаления просроченных задач
     */
    public TaskRemoveExpiredListener(TaskRemoveExpiredService taskRemoveExpiredService,UserListener userListener,ScheduledExecutorService executorService,CommandHistoryService commandHistoryService) {
        super(commandHistoryService);
        this.taskRemoveExpiredService = taskRemoveExpiredService;
        this.userListener = userListener;
        this.executorService = executorService;
    }

    /**
     * Получить текущего пользователя
     * @return текущий пользователь
     */
    @Override
    public User getUser() {
        return userListener.getUser();
    }
    /**
     * Единственный экземпляр объекта TaskWarningListener
     */
    private static TaskRemoveExpiredListener instance = null;
    /**
     * Получить единственный экземпляр объекта TaskWarningService
     * @return единственный экземпляр объекта TaskWarningService
     */
    public static TaskRemoveExpiredListener getInstance(){
        if (instance == null){
            instance = new TaskRemoveExpiredListener();
        }
        return instance;
    }
    /**
     * Оповестить слушателя о новой комманде
     *
     * @param command комманда
     * @return 0 не обработана комманда 1 обработана
     */
    @Override
    public int notify(String command) {
        switch (command) {
            case SHORT_USER_AUTH:
            case USER_AUTH:return startRemoveExpiredTasks();
            case SHORT_EXIT:
            case EXIT:;
            case SHORT_USER_END:
            case USER_END:return stopRemoveExpiredTasks();
            default:return -1;
        }
    }
    /**
     * Оповещение пользователей
     */
    public void removeExpiredTasks() {
        if(getUser()==null) return;
        if(getUser().getId()==null) return;
        taskRemoveExpiredService.removeExpiredTasks(getUser().getId());
    }
    /**
     * Запуск процесса
     */
    public int startRemoveExpiredTasks() {
        if(scheduledFuture!=null) {
            return 0;
        }
        scheduledFuture = executorService.scheduleAtFixedRate(this::removeExpiredTasks, 0, 30, TimeUnit.SECONDS);
        return 0;
    }
    /**
     * Остановка процесса
     */
    public int stopRemoveExpiredTasks() {
        if(scheduledFuture==null) {
            return 0;
        }
        scheduledFuture.cancel(true);
        scheduledFuture = null;
        executorService.shutdownNow();
        return 0;
    }


}

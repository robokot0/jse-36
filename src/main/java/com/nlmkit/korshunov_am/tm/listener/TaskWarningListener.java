package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.TaskWarningService;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.*;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.USER_END;

public class TaskWarningListener extends AbstractListener implements Listener {
    /**
     * Ссылка на listener ользователей
     */
    UserListener userListener;

    /**
     * Ссылка на Executor
     */
    ScheduledExecutorService executorService;
    /**
     * Ссылка на запуск по расписанию
     */
    ScheduledFuture scheduledFuture = null;
    /**
     * Сервис оповещений по задачам
     */
    TaskWarningService taskWarningService;
    /**
     * Приватный кконструктор по умолчанию
     */
    private TaskWarningListener() {
        super(CommandHistoryService.getInstance());
        this.taskWarningService = TaskWarningService.getInstance();
        this.userListener = UserListener.getInstance();
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }
    /**
     * Конструктор длоя тестирвоания
     * @param taskWarningService Сервис оповещений
     */
    public TaskWarningListener(TaskWarningService taskWarningService,UserListener userListener,ScheduledExecutorService executorService,CommandHistoryService commandHistoryService) {
        super(commandHistoryService);
        this.taskWarningService = taskWarningService;
        this.userListener = userListener;
        this.executorService = executorService;
    }

    /**
     * Получить текущего пользователя
     *
     * @return текущий пользователь
     */
    @Override
    public User getUser() {
        return userListener.getUser();
    }
    /**
     * Единственный экземпляр объекта TaskWarningListener
     */
    private static TaskWarningListener instance = null;
    /**
     * Получить единственный экземпляр объекта TaskWarningService
     * @return единственный экземпляр объекта TaskWarningService
     */
    public static TaskWarningListener getInstance(){
        if (instance == null){
            instance = new TaskWarningListener();
        }
        return instance;
    }
    /**
     * Оповестить слушателя о новой комманде
     *
     * @param command комманда
     * @return 0 не обработана комманда 1 обработана
     */
    @Override
    public int notify(String command) {
        switch (command) {
            case SHORT_USER_AUTH:
            case USER_AUTH:return startShowWarnings();
            case SHORT_EXIT:
            case EXIT:;
            case SHORT_USER_END:
            case USER_END:return stopShowWarnings();
            default:return -1;
        }
    }
    /**
     * Оповещение пользователей
     */
    public void ShowWarnings() {
        if(getUser()==null) return;
        if(getUser().getId()==null) return;
        LocalDateTime localDateTime = LocalDateTime.now();
        List<Task> taskList = taskWarningService.findTaskForWarning(getUser().getId());
        for (final Task task: taskList) {
            inputConsole.showMessage("Until the end of the task " + task.getName() + " is left " + (task.getDeadLine()==null?"":localDateTime.until(task.getDeadLine(), ChronoUnit.MINUTES)) + "(id=" + task.getId()+ ")");
        }
    }
    /**
     * Запуск процесса оповещения пользователей об оконачении срока
     */
    public int startShowWarnings() {
        if(scheduledFuture!=null) {
            return 0;
        }
        scheduledFuture = executorService.scheduleAtFixedRate(this::ShowWarnings, 0, 30, TimeUnit.SECONDS);
        return 0;
    }
    /**
     * Остановка процесса оповещения пользователей об оконачении срока
     */
    public int stopShowWarnings() {
        if(scheduledFuture==null) {
            return 0;
        }
        scheduledFuture.cancel(true);
        scheduledFuture = null;
        executorService.shutdownNow();
        return 0;
    }
}

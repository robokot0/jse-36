package com.nlmkit.korshunov_am.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.UserService;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;

public class UserListener extends AbstractListener implements Listener {
    /**
     * Текущий пользователь
     */
    private User user=null;
    /**
     * Сервис задач
     */
    private final UserService userService;
    /**
     * Публичный конструктор для тестов
     */
    UserListener(UserService userService, CommandHistoryService commandHistoryService)  {
        super(commandHistoryService);
        this.userService = userService;
    }
    /**
     * Приватный конструктор по умолчанию
     */
    private UserListener()  {
        super(CommandHistoryService.getInstance());
        userService = UserService.getInstance();
    }
    /**
     * Единственный экземпляр объекта UserListener
     */
    private static UserListener instance = null;

    /**
     * Получить единственный экземпляр объекта UserListener
     * @return единственный экземпляр объекта UserListener
     */
    public static UserListener getInstance()  {
        if (instance == null){
            instance = new UserListener();
        }
        return instance;
    }



    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_USER_CREATE:
                case USER_CREATE:return createUser();
                case SHORT_USER_LIST:
                case USER_LIST:return listUser();
                case SHORT_USER_VIEW_BY_ID:
                case USER_VIEW_BY_ID:return viewUserById();
                case SHORT_USER_VIEW_BY_INDEX:
                case USER_VIEW_BY_INDEX:return viewUserByIndex();
                case SHORT_USER_VIEW_BY_LOGIN:
                case USER_VIEW_BY_LOGIN:return viewUserByLogin();
                case SHORT_USER_REMOVE_BY_ID:
                case USER_REMOVE_BY_ID:return removeUserById();
                case SHORT_USER_REMOVE_BY_INDEX:
                case USER_REMOVE_BY_INDEX:return removeUserByIndex();
                case SHORT_USER_REMOVE_BY_LOGIN:
                case USER_REMOVE_BY_LOGIN:return removeUserByLogin();
                case SHORT_USER_UPDATE_BY_ID:
                case USER_UPDATE_BY_ID:return updateUserDataById();
                case SHORT_USER_UPDATE_BY_INDEX:
                case USER_UPDATE_BY_INDEX:return updateUserDataByIndex();
                case SHORT_USER_UPDATE_BY_LOGIN:
                case USER_UPDATE_BY_LOGIN:return updateUserDataByLogin();
                case SHORT_USER_UPDATE_PASSWORD_BY_ID:
                case USER_UPDATE_PASSWORD_BY_ID:return updateUserPasswordById();
                case SHORT_USER_UPDATE_PASSWORD_BY_INDEX:
                case USER_UPDATE_PASSWORD_BY_INDEX:return updateUserPasswordByIndex();
                case SHORT_USER_UPDATE_PASSWORD_BY_LOGIN:
                case USER_UPDATE_PASSWORD_BY_LOGIN:return updateUserPasswordByLogin();
                case SHORT_USER_AUTH:
                case USER_AUTH:return authUser();
                case SHORT_USER_UPDATE_PASSWORD:
                case USER_UPDATE_PASSWORD:return updateAuthUserPassword();
                case SHORT_USER_VIEW:
                case USER_VIEW:return viewAuthUser();
                case SHORT_USER_UPDATE:
                case USER_UPDATE:return updateAuthUser();
                case SHORT_USER_END:
                case USER_END:return endAuthUserSession();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return saveAsJSON();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return saveAsXML();
                case SHORT_LOAD_FROM_XML:
                case LOAD_FROM_XML: return loadFromXML();
                case SHORT_LOAD_FROM_JSON:
                case LOAD_FROM_JSON: return loadFromJSON();
                case SHORT_HELP:
                case HELP: return displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            ShowResult("[FAIL] "+e.getMessage());
        }
        return 0;
    }

    /**
     * Получить текущего пользователя
     * @return текущий пользователь
     */
    @Override
    public User getUser() {
        return user;
    }
    /**
     * Задать текущего пользователя
     * @param user пользователь
     */
    public void setUser(User user) {
        this.user=user;
    }
    /**
     * Аутентификация пользователя
     * @return 0 выполнено
     */
    public int authUser() throws WrongArgumentException {
        final String login = enterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String password = enterPasswordCommandParameter("user password");
        if (!userService.getStringHash(password).equals(user.getPasswordHash())){
            ShowResult("[FAIL]");
            return 0;
        }
        this.setUser(user);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Смена пароля аутентифицировавшегося пользователя
     * @return 0 выполнено
     */
    public int updateAuthUserPassword() throws WrongArgumentException {
        this.updateUserPassword(this.getUser());
        return 0;
    }
    /**
     * Показать данные аутентифицированного пользователя
     * @return 0 выполнено
     */
    public int viewAuthUser(){
        this.viewUser(this.getUser());
        return 0;
    }
    /**
     * Изменить данные аутентифицировавшегося пользователя без изменения роли
     * @return 0 выполнено
     */
    public int updateAuthUser() throws WrongArgumentException {
        updateUserDataNoRole(this.getUser());
        return 0;
    }
    public int endAuthUserSession(){
        this.setUser(null);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     */
    public void updateUserDataNoRole(final User user) throws WrongArgumentException {
        if (user == null) {
            ShowResult("[FAIL]");
            return;
        }
        final String login = enterStringCommandParameter("user login");
        final String firstName = enterStringCommandParameter("user first name");
        final String secondName = enterStringCommandParameter("user second name");
        final String middleName = enterStringCommandParameter("user middle name");
        userService.updateData(user.getId(),login,user.getRole(),firstName,secondName,middleName);
        ShowResult("[OK]");
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     */
    public void updateUserData(final User user) throws WrongArgumentException {
        if(user == null){
            ShowResult("[FAIL]");
            return;
        }
        final String login = enterStringCommandParameter("user login");
        final Role role = Role.valueOf(enterStringCommandParameter("user role"));
        final String firstName = enterStringCommandParameter("user first name");
        final String secondName = enterStringCommandParameter("user second name");
        final String middleName = enterStringCommandParameter("user middle name");
        userService.updateData(user.getId(),login,role,firstName,secondName,middleName);
        ShowResult("[OK]");
    }
    /**
     * Изменить пароль пользователя
     * @param user пользователь
     */
    public void updateUserPassword(final User user) throws WrongArgumentException {
        if (user == null) {
            ShowResult("[FAIL]");
            return;
        }
        final String password = enterPasswordCommandParameter("user password");
        final String passwordConfirmation = enterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return;
        }
        userService.updatePassword(user.getId(),userService.getStringHash(password));
        ShowResult("[OK]");
    }
    /**
     * Изменить данные пользователя по login
     * @return 0 выполнено
     */
    public int updateUserDataByLogin() throws WrongArgumentException {
        System.out.println("[UPDATE USER DATA BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по login
     * @return 0 выполнено
     */
    public int updateUserPasswordByLogin() throws WrongArgumentException {
        System.out.println("[UPDATE USER PASSWORD BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по id
     * @return 0 выполнено
     */
    public int updateUserDataById() throws WrongArgumentException {
        System.out.println("[UPDATE USER DATA BY ID]");
        final long id = enterLongCommandParameter("user ID");
        final User user = userService.findById(id);
        if (user == null) {
            ShowResult("[FAIL]");
        } else {
            updateUserData(user);
        }
        return 0;
    }
    /**
     * Изменить пароль пользователя по id
     * @return 0 выполнено
     */
    public int updateUserPasswordById() throws WrongArgumentException {
        System.out.println("[UPDATE USER PASSWORD BY ID]");
        final long id = enterLongCommandParameter("user ID");
        final User user = userService.findById(id);
        if (user == null) {
            ShowResult("[FAIL]");
        } else {
            updateUserPassword(user);
        }
        return 0;
    }
    /**
     * Изменить данные пользователя по index
     * @return 0 выполнено
     */
    public int updateUserDataByIndex() throws WrongArgumentException {
        System.out.println("[UPDATE USER DATA BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = enterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            ShowResult("[FAIL]");
        } else {
            updateUserData(user);
        }
        return 0;
    }
    /**
     * Изменить пароль пользователя по index
     * @return 0 выполнено
     */
    public int updateUserPasswordByIndex() throws WrongArgumentException {
        System.out.println("[UPDATE USER PASSWORD BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = enterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        if (user == null) {
            ShowResult("[FAIL]");
        } else {
            updateUserPassword(user);
        }
        return 0;
    }
    /**
     * Удалить пользователя по login
     * @return 0 выполнено
     */
    public int removeUserByLogin() throws WrongArgumentException {
        System.out.println("[REMOVE USER BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final User userfinded = userService.findByLogin(login);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) {
            ShowResult("[FAIL]");
        } else {
            ShowResult("[OK]");
        }
        return 0;
    }
    /**
     * Удалить пользователя по id
     * @return 0 выполнено
     */
    public int removeUserById() throws WrongArgumentException {
        System.out.println("[REMOVE USER BY ID]");
        if (!this.testAdminUser())return 0;
        final long id = enterLongCommandParameter("user ID");
        final User userdeleted = userService.removeById(id);
        if (userdeleted == null) {
            ShowResult("[FAIL]");
        } else {
            ShowResult("[OK]");
        }
        return 0;
    }
    /**
     * Удалить пользователя по index
     * @return 0 выполнено
     */
    public int removeUserByIndex() throws WrongArgumentException {
        System.out.println("[REMOVE USER BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = enterIntegerCommandParameter("user index")-1;
        final User userfinded = userService.findByIndex(index);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) {
            ShowResult("[FAIL]");
        } else {
            ShowResult("[OK]");
        }
        return 0;
    }
    /**
     * Создать пользователя
     * @return 0 выполнено
     */
    public int createUser() throws WrongArgumentException {
        System.out.println("[CREATE USER]");
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final Role role = Role.valueOf(enterStringCommandParameter("user role"));
        final String firstName = enterStringCommandParameter("user first name");
        final String secondName = enterStringCommandParameter("user second name");
        final String middleName = enterStringCommandParameter("user middle name");
        final String password = enterPasswordCommandParameter("user password");
        final String passwordConfirmation = enterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return 0;
        }
        userService.create(login,role,firstName,secondName,middleName,userService.getStringHash(password));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать информацию по пользователю
     * @param user пользователь
     */
    public void viewUser(final User user) {
        if (user == null){
            ShowResult("[FAIL]");
            return;
        }
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().toString());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("PASSWORD HASH: " + user.getPasswordHash());
        ShowResult("[OK]");
    }
    /**
     * Показать пользователя по login
     * @return 0 выполнено
     */
    public int viewUserByLogin() throws WrongArgumentException {
        if (!this.testAdminUser())return 0;
        final String login = enterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по id
     * @return 0 выполнено
     */
    public int viewUserById() throws WrongArgumentException {
        if (!this.testAdminUser())return 0;
        final long id = enterLongCommandParameter("user ID");
        final  User user = userService.findById(id);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по index
     * @return 0 выполнено
     */
    public int viewUserByIndex() throws WrongArgumentException {
        if (!this.testAdminUser())return 0;
        final int index = enterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }
    /**
     * Показать список пользователей
     * @param users список
     */
    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". " + user.getLogin()+ ": " + user.getRole()+ ": " + user.getFirstName()+ ": " + user.getSecondName()+ ": " + user.getMiddleName());
            index ++;
        }
    }
    /**
     * Показать список пользователей
     * @return 0 выполнено
     */
    public int listUser(){
        System.out.println("[LIST USER]");
        if (!this.testAdminUser())return 0;
        viewUsers(userService.findAll());
        ShowResult("[OK]");
        return 0;
    }


    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        System.out.println("----User commands:");
        System.out.println("user-create - Create user. (uc)");
        System.out.println("user-list - View list of users. (ul)");
        System.out.println("user-view-by-id - View user data by id. (uvid)");
        System.out.println("user-view-by-index - View user data by index. (uvi)");
        System.out.println("user-view-by-login - View user data by login. (uvl)");
        System.out.println("user-remove-by-id - Remove user by id. (urid)");
        System.out.println("user-remove-by-index - Remove user by index. (uri)");
        System.out.println("user-remove-by-login - Remove user by login. (url)");
        System.out.println("user-update-by-id - Update user data by id. (uuid)");
        System.out.println("user-update-by-index - Update user data by index. (uui)");
        System.out.println("user-update-by-login - Update user data by login. (uul)");
        System.out.println("user-update-password-by-id - Change password of user by id. (uupid)");
        System.out.println("user-update-password-by-index - Change password of user by index. (uupi)");
        System.out.println("user-update-password-by-login - Change password of user by login. (uupl)");
        System.out.println("user-auth - Auth user. (ua)");
        System.out.println("user-update-password - Update password of auth user. (uup)");
        System.out.println("user-view - View auth user data. (uv)");
        System.out.println("user-update - Update auth user data. (uu)");
        System.out.println("user-end-session - End auth user session. (ue)");
        System.out.println("user-of-project-set-by-index - Set user of project. Project find by index (up)");
        System.out.println("user-of-task-set-by-index - Set user of task. Task find by index. (ut)");
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Сохранить как JSON
     * @throws IOException ошибка ввода вывода
     */
    public int saveAsJSON() throws IOException {
        userService.saveAs( new ObjectMapper(),new FileOutputStream(this.getClass().getName()+".json"));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Сохранить как XML
     * @throws IOException ошибка ввода вывода
     */
    public int saveAsXML() throws IOException {
        userService.saveAs( new XmlMapper(),new FileOutputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Загрузить из XML
     * @throws IOException ошибка ввода вывода
     */
    public int loadFromXML()  throws IOException {
        userService.loadFrom(new XmlMapper(), new FileInputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Загрузить из JSON
     * @throws IOException ошибка ввода вывода
     */
    public int loadFromJSON()  throws IOException {
        userService.loadFrom(new ObjectMapper(), new FileInputStream(this.getClass().getName()+".xml"));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Найти пользователя по login.
     * @param login login
     * @return пользователь
     */
    public User findByLogin(String login) {
        return userService.findByLogin(login);
    }

    /**
     * Найти пользователя по id.
     * @param id id
     * @return пользователь
     */
    public User findById(Long id) {
        return userService.findById(id);
    }
}

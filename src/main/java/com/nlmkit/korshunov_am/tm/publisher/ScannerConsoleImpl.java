package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class ScannerConsoleImpl implements InputConsole{
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Сканер
     */
    Scanner scanner ;
    /**
     * Замена сканера для тестирования
     */
    public void setScanner(Scanner scanner){
        this.scanner = scanner;
    }
    /**
     * Приватный конструктор по умолчанию
     */
    private ScannerConsoleImpl(){
        super();
        this.scanner = new Scanner(System.in);
    }
    /**
     * Единственный экземпляр объекта CommandHistoryService
     */
    private static ScannerConsoleImpl instance = null;
    /**
     * Получить единственный экземпляр объекта
     * @return единственный экземпляр объекта
     */
    public static ScannerConsoleImpl getInstance(){
        if (instance == null){
            instance = new ScannerConsoleImpl();
        }
        return instance;
    }
    @Override
    public String getNextInputString(String parameterName,Boolean throwEmpty) throws WrongArgumentException {
        showMessage("Please enter "+parameterName+": ");
        String parametervalue=scanner.nextLine();
        if (parametervalue.isEmpty()) {
            if(throwEmpty) {
                throw new WrongArgumentException("No value set to " + parameterName);
            } else {
                return null;
            }
        }
        return parametervalue;
    }
    @Override
    public Integer getNextInputInteger(String parameterName,Boolean throwEmpty)  throws WrongArgumentException  {
        String result;
        try {
            result = getNextInputString(parameterName, throwEmpty);
            if(result == null){
                return null;
            }
            return Integer.parseInt(result);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Integer " + parameterName + " from " + parameterName);
        }
    }
    @Override
    public Long getNextInputLong(String parameterName,Boolean throwEmpty)  throws WrongArgumentException {
        String result = "";
        try {
            result = getNextInputString(parameterName,throwEmpty);
            if(result == null){
                return null;
            }
            return Long.parseLong(result);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get Long " + parameterName + " from " + result);
        }
    }
    @Override
    public String getNextInputPassword(String parameterName,Boolean throwEmpty)  throws WrongArgumentException {
        return getNextInputString(parameterName,throwEmpty);
    }

    /**
     * Получить дату время
     *
     * @param parameterName сообщение с пояснением
     * @param throwEmpty    exception еслине задано
     * @return Введенное значение
     * @throws WrongArgumentException Неправильнрый аргумент
     */
    @Override
    public LocalDateTime getNextInputLocalDateTime(String parameterName, Boolean throwEmpty) throws WrongArgumentException {
        String result="";
        result = getNextInputString(parameterName+" (offset in hours from now)",throwEmpty);
        if(result == null){
            return null;
        }
        try {
            Integer offsetHours = Integer.parseInt(result);
            return LocalDateTime.now().plusHours(offsetHours).truncatedTo(ChronoUnit.SECONDS);
        } catch (NumberFormatException e) {
            throw new WrongArgumentException("Failed to get offset in hours from now to enter " + parameterName + " from " + result);
        }
    }

    @Override
    public void showMessage(String message) {
        logger.info(message);
    }
}

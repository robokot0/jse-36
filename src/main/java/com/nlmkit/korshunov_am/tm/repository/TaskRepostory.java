package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Task;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Репозитарий задач
 */
public class TaskRepostory  extends AbstractRepository <Task> {
    /**
     * Приватный конструктор по умолчанию
     */
    private TaskRepostory(){
    }
    /**
     * Единственный экземпляр объекта TaskRepostory
     */
    private static TaskRepostory instance = null;

    /**
     * Получить единственный экземпляр объекта TaskRepostory
     * @return единственный экземпляр объекта TaskRepostory
     */
    public static TaskRepostory getInstance(){
        if (instance == null){
            instance = new TaskRepostory();
        }
        return instance;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(String name,final Long userId) {
        final Task task = new Task(name,userId);
        return create(task);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @param deadLine срок исполнения задачи
     * @return задача
     */
    public Task create(final String name, final String description, final Long userId, LocalDateTime deadLine) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        task.setDeadLine(deadLine);
        return create(task);
    }

    /**
     * Изменить сущность
     * @param id Идентификатор
     * @param name Имя
     * @param description Описание
     * @param deadLine Срок исполнения
     * @return проект
     */
    public Task update(final Long id,final String name,final String description,final Long userId,final LocalDateTime deadLine) {
        Task task = super.update(id,name,description,userId);
        task.setDeadLine(deadLine);
        return task;
    }

    /**
     * Ищем задачу по идентификатору проекта и по идентификатору задачи
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id){
        final Task task = findById(id);
        if (task == null) {
            return null;
        }
        final Long idProject = task.getProjectId();
        if (idProject == null) {
            return null;
        }
        if (!idProject.equals(projectId)) {
            return null;
        }
        return task;
    }
    /**
     * Ищем задачу по идентификатору проекта идентификатору задачи и пользователю.
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id,final Long userId){
        final Task task= findByProjectIdAndId(projectId,id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     */
    public void findAddByProjectId(final Long projectId, final Long taskId) {
        final Task task = findById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * задача должны быть указанного пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     */
    public void findAddByProjectId(final Long projectId, final Long taskId, final Long userId) {
        Task task = findById(taskId,userId);
        if(task==null) return;
        task.setProjectId(projectId);
    }

    /**
     * Получить список задач проекта
     * @param projectId идентрификатор проектв
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентрификатор проектв
     * @param userId ид пользователя
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : entityes.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                final Long idProject = task.getProjectId();
                if (idProject == null) continue;
                if (!task.getUserId().equals(userId))continue;
                if (projectId.equals(idProject)) result.add(task);
            }
        }
        return result;
    }
    /**
     * Получить список задач с учетом пользователя и со сроком окончания задачи между указанными датами
     * @param fromTime со времени
     * @param toTime по время
     * @param userId ид пользователя
     * @return списрк задач
     */
    public List<Task> findAllByDeadLinePeriod(final LocalDateTime fromTime,final LocalDateTime toTime,final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (Map.Entry<String, List<Task>> entry : entityes.entrySet()) {
            List<Task> taskList = entry.getValue();
            for (final Task task: taskList){
                if (!task.getUserId().equals(userId)) {
                    continue;
                }
                LocalDateTime deadLine = task.getDeadLine();
                if (deadLine==null) {
                    continue;
                }
                if (deadLine.compareTo(fromTime)<=0 || deadLine.compareTo(toTime)>=0){
                    continue;
                }
                result.add(task);
            }
        }
        return result;
    }
    public  Class getEntityClass(){return Task.class;};



}

package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class SystemService extends AbstractService  {
    ProjectRepository projectRepository;
    UserRepository userRepository;
    TaskRepostory taskRepostory;
    UserService userService;
    /**
     * Приватный конструктор по умолчанию
     */
    private SystemService(){
        super();
        this.projectRepository=null;
        this.userRepository=null;
        this.taskRepostory=null;
        this.userService=null;
    }
    /**
     * Публичный конструктор для тестирования
     */
    public SystemService(ProjectRepository projectRepository, UserRepository userRepository,TaskRepostory taskRepostory,UserService userService){
        super();
        this.projectRepository=projectRepository;
        this.userRepository=userRepository;
        this.taskRepostory=taskRepostory;
        this.userService=userService;
    }
    /**
     * Единственный экземпляр объекта AbstractService
     */
    private static SystemService instance = null;

    /**
     * Получить единственный экземпляр объекта AbstractService
     * @return единственный экземпляр объекта AbstractService
     */
    public static SystemService getInstance(){
        if (instance == null){
            instance = new SystemService();
        }
        return instance;
    }

    public void loadTestData() {
        final ProjectRepository projectRepository= this.projectRepository==null?ProjectRepository.getInstance():this.projectRepository;
        final UserRepository userRepository= this.userRepository==null?UserRepository.getInstance():this.userRepository;
        final TaskRepostory taskRepostory= this.taskRepostory==null?TaskRepostory.getInstance():this.taskRepostory;
        final UserService userService= this.userService==null?UserService.getInstance():this.userService;

        projectRepository.clear();
        userRepository.clear();
        taskRepostory.clear();

        final User user1=userRepository.create("USER", Role.valueOf("USER"),"FN","SN","MN",userService.getStringHash("123"));
        final User user2=userRepository.create("USER1",Role.valueOf("USER"),"FN1","SN1","MN1",userService.getStringHash("123"));
        userRepository.create("ADMIN",Role.valueOf("ADMIN"),"FN","SN","MN",userService.getStringHash("123"));

        final Project project1 = projectRepository.create("P2","D1",user1.getId());
        final Project project2 = projectRepository.create("P1","D2",user2.getId());

        final Task task1 = taskRepostory.create("T3","D1",user1.getId(), LocalDateTime.now().plusMinutes(31).truncatedTo(ChronoUnit.SECONDS));
        final Task task2 = taskRepostory.create("T2","D2",user2.getId(), LocalDateTime.now().plusMinutes(16).truncatedTo(ChronoUnit.SECONDS));
        final Task task4 = taskRepostory.create("T4","D4",user1.getId(), LocalDateTime.now().plusMinutes(6).truncatedTo(ChronoUnit.SECONDS));
        taskRepostory.create("T1","D3",user1.getId(), LocalDateTime.now().plusMinutes(16).truncatedTo(ChronoUnit.SECONDS));

        taskRepostory.findAddByProjectId(project1.getId(),task1.getId());
        taskRepostory.findAddByProjectId(project2.getId(),task2.getId());
        taskRepostory.findAddByProjectId(project1.getId(),task4.getId());
    }

}

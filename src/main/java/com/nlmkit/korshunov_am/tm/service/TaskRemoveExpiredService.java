package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class TaskRemoveExpiredService {
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(this.getClass().getName());
    /**
     * Репозитарий задач
     */
    TaskRepostory taskRepostory;
    /**
     * Приватный кконструктор по умолчанию
     */
    private TaskRemoveExpiredService() {
        this.taskRepostory = TaskRepostory.getInstance();
    }
    /**
     * Конструктор длоя тестирвоания
     * @param taskRepostory Репозитарий задач
     */
    public TaskRemoveExpiredService(TaskRepostory taskRepostory) {
        this.taskRepostory = taskRepostory;
    }
    /**
     * Единственный экземпляр объекта TaskWarningService
     */
    private static TaskRemoveExpiredService instance = null;

    /**
     * Получить единственный экземпляр объекта TaskWarningService
     * @return единственный экземпляр объекта TaskWarningService
     */
    public static TaskRemoveExpiredService getInstance(){
        if (instance == null){
            instance = new TaskRemoveExpiredService();
        }
        return instance;
    }

    public void removeExpiredTasks(final Long userId) {
        logger.debug("RemoveExpiredTasks({})",userId);
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        logger.debug("RemoveExpiredTasks:now={}",now);
        List<Task> taskList = taskRepostory.findAllByDeadLinePeriod(LocalDateTime.MIN,now.minusSeconds(30),userId);
        taskList.forEach(i ->taskRepostory.removeById(i.getId()));
    }

}

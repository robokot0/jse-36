package com.nlmkit.korshunov_am.tm.entity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CommandTest {
    /**
     * Проверка конструктора
     */
    @Test
    public void testGetCommandText() {
        Command command = new Command("testcommand");
        assertEquals(command.getCommandText(),"testcommand");
    }
    /**
     * Проверка назначения текста
     */
    @Test
    public void testSetCommandText() {
        Command command = new Command("testcommand1");
        command.setCommandText("testcommand");
        assertEquals(command.getCommandText(),"testcommand");
    }
    /**
     * Проверка добавления текста в комманду
     */
    @Test
    public void testAddCommandText1() {
        Command command = new Command("testcommand");
        command.addCommandText("aaaa");
        assertEquals(command.getCommandText(),"testcommand aaaa");
    }
    /**
     * Проверка добавления пустого текста в комманду
     */
    @Test
    public void testAddCommandText2() {
        Command command = new Command("testcommand");
        command.addCommandText("");
        assertEquals(command.getCommandText(),"testcommand");
    }
    /**
     * Проверка добавления текста в пустую комманду
     */
    @Test
    public void testAddCommandText3() {
        Command command = new Command("");
        command.addCommandText("aaa");
        assertEquals(command.getCommandText(),"aaa");
    }
}
package com.nlmkit.korshunov_am.tm.entity;

import com.nlmkit.korshunov_am.tm.enumerated.Role;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Test
    public void getIdsetId() {
        User user = new User();
        user.setId(Long.MAX_VALUE);
        assertEquals(user.getId().longValue(),Long.MAX_VALUE);
    }

    @Test
    public void getLoginsetLogin() {
        User user = new User("bbb");
        assertEquals(user.getLogin(),"bbb");
        user.setLogin("aaa");
        assertEquals(user.getLogin(),"aaa");
    }

    @Test
    public void getRolesetRole() {
        User user = new User();
        user.setRole(Role.ADMIN);
        assertEquals(user.getRole().toString(),Role.ADMIN.toString());
    }

    @Test
    public void getPasswordHashsetPasswordHash() {
        User user = new User();
        user.setPasswordHash("11111");
        assertEquals(user.getPasswordHash(),"11111");
    }

    @Test
    public void getFirstNamesetFirstName() {
        User user = new User();
        user.setFirstName("aaaa");
        assertEquals(user.getFirstName(),"aaaa");
    }

    @Test
    public void getSecondNamesetSecondName() {
        User user = new User();
        user.setSecondName("aaa");
        assertEquals(user.getSecondName(),"aaa");
    }

    @Test
    public void setMiddleNamesetMiddleName() {
        User user = new User();
        user.setMiddleName("sss");
        assertEquals(user.getMiddleName(),"sss");
    }

    @Test
    public void isAdmin() {
        User user = new User();
        user.setRole(Role.ADMIN);
        assertTrue(user.isAdmin());
        user.setRole(Role.USER);
        assertFalse(user.isAdmin());
    }
}
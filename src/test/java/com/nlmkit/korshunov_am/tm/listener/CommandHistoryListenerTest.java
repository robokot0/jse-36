package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CommandHistoryListenerTest {
    static CommandHistoryListener commandHistoryListener;
    static CommandHistoryService commandHistoryService;
    static InputConsole inputConsole;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        commandHistoryListener = new CommandHistoryListener(commandHistoryService);
        commandHistoryListener.setConsole(inputConsole);
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            commandHistoryListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            commandHistoryListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,commandHistoryListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,commandHistoryListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,commandHistoryListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,commandHistoryListener.enterLongCommandParameter("pn5"));
        });
    }
    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            commandHistoryListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void getInstance() {
        CommandHistoryListener commandHistoryListener = CommandHistoryListener.getInstance();
        assertEquals(commandHistoryListener,CommandHistoryListener.getInstance());
    }

    @Test
    void testNotify() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(new Command("command"));
        doReturn(commandList).when(commandHistoryService).findAll();
        assertEquals(-1,commandHistoryListener.notify("aaaaaa"));
        assertEquals(0,commandHistoryListener.notify(SHORT_HELP));
        assertEquals(0,commandHistoryListener.notify(HELP));
        assertEquals(0,commandHistoryListener.notify(SHORT_COMMAND_HISTORY_VIEW));
        assertEquals(0,commandHistoryListener.notify(COMMAND_HISTORY_VIEW));
    }

    @Test
    void addCommandToHistory() {
        commandHistoryListener.addCommandToHistory("command");
        verify(commandHistoryService,times(1)).addCommandToHistory("command");
    }
}
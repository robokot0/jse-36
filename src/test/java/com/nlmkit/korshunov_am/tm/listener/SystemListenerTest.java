package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.SystemService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class SystemListenerTest {
    static SystemListener systemListener;
    static SystemService systemService;
    static CommandHistoryService commandHistoryService;
    static InputConsole inputConsole;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        systemService = Mockito.mock(SystemService.class);
        systemListener = new SystemListener(commandHistoryService,systemService);
        systemListener.setConsole(inputConsole);
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            systemListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            systemListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,systemListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,systemListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,systemListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,systemListener.enterLongCommandParameter("pn5"));
        });
    }

    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            systemListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void getInstance() {
        SystemListener systemListener = SystemListener.getInstance();
        assertEquals(systemListener,SystemListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,systemListener.notify("aaaaaa"));
        assertEquals(0,systemListener.notify(SHORT_VERSION));
        assertEquals(0,systemListener.notify(VERSION));
        assertEquals(0,systemListener.notify(SHORT_ABOUT));
        assertEquals(0,systemListener.notify(ABOUT));
        assertEquals(0,systemListener.notify(SHORT_HELP));
        assertEquals(0,systemListener.notify(HELP));
        assertEquals(0,systemListener.notify(SHORT_EXIT));
        assertEquals(0,systemListener.notify(EXIT));
        assertEquals(0,systemListener.notify(SHORT_LOAD_TEST_DATA));
        assertEquals(0,systemListener.notify(LOAD_TEST_DATA));
    }

    @Test
    void getUser() {
        assertNull(systemListener.getUser());
    }

}
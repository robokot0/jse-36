package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskListenerTest {
    static TaskListener taskListener;
    static TaskService taskService;
    static InputConsole inputConsole;
    static ProjectTaskService projectTaskService;
    static CommandHistoryService commandHistoryService;
    static UserListener userListener;
    static User user;

    @BeforeAll
    public static void doBeforeAll() {
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        taskService = Mockito.mock(TaskService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        projectTaskService =  Mockito.mock(ProjectTaskService.class);
        userListener = Mockito.mock(UserListener.class);
        taskListener = new TaskListener(taskService,projectTaskService,commandHistoryService,userListener);
        taskListener.setConsole(inputConsole);
        user = new User("login");
    }

    @Test
    void setUsergetUser() {
        assertEquals(user,taskListener.getUser());
    }

    @Test
    void enterStringCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputString("pn",false);
            taskListener.enterStringCommandParameter("pn");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn","pv");
        });
    }

    @Test
    void enterPasswordCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("pv").when(inputConsole).getNextInputPassword("pn1",false);
            taskListener.enterPasswordCommandParameter("pn1");
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn1","******");
        });
    }

    @Test
    void enterIntegerCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn2",true);
            assertEquals(1,taskListener.enterIntegerCommandParameter("pn2"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn2","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn3",true);
            assertEquals(1,taskListener.enterIntegerCommandParameter("pn3"));
        });
    }

    @Test
    void enterLongCommandParameter() {
        assertDoesNotThrow(()-> {
            doReturn("1").when(inputConsole).getNextInputString("pn4",true);
            assertEquals(1L,taskListener.enterLongCommandParameter("pn4"));
            verify(commandHistoryService,times(1)).AddCommandParameterToLastCommand("pn4","1");
        });
        assertThrows(WrongArgumentException.class,()->{
            doReturn("a").when(inputConsole).getNextInputString("pn5",true);
            assertEquals(1L,taskListener.enterLongCommandParameter("pn5"));
        });
    }

    @Test
    void showResult() {
        assertDoesNotThrow(()-> {
            taskListener.ShowResult("result");
            verify(inputConsole,times(1)).showMessage("result");
            verify(commandHistoryService,times(1)).AddCommandResultToLastCommand("result");
        });
    }

    @Test
    void testAuthUser() {
        doReturn(user).when(userListener).getUser();
        assertTrue(taskListener.testAuthUser());
        doReturn(null).when(userListener).getUser();
        assertFalse(taskListener.testAuthUser());
    }

    @Test
    void testAdminUser() {
        User user = new User("testAdminUser");
        user.setRole(Role.ADMIN);
        doReturn(user).when(userListener).getUser();
        assertTrue(taskListener.testAdminUser());
        user.setRole(Role.USER);
        assertFalse(taskListener.testAdminUser());
        doReturn(null).when(userListener).getUser();
        assertFalse(taskListener.testAdminUser());
    }

    @Test
    void getInstance() {
        TaskListener taskListener = TaskListener.getInstance();
        assertEquals(taskListener,TaskListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,taskListener.notify("aaaaaa"));
        assertEquals(0,taskListener.notify(SHORT_TASK_CREATE));
        assertEquals(0,taskListener.notify(TASK_CREATE));
        assertEquals(0,taskListener.notify(SHORT_TASK_CLEAR));
        assertEquals(0,taskListener.notify(TASK_CLEAR));
        assertEquals(0,taskListener.notify(SHORT_TASK_LIST));
        assertEquals(0,taskListener.notify(TASK_LIST));
        assertEquals(0,taskListener.notify(SHORT_TASK_VIEW));
        assertEquals(0,taskListener.notify(TASK_VIEW));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_ID));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_ID));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_NAME));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_NAME));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_BY_INDEX));
        assertEquals(0,taskListener.notify(TASK_REMOVE_BY_INDEX));
        assertEquals(0,taskListener.notify(SHORT_TASK_UPDATE_BY_INDEX));
        assertEquals(0,taskListener.notify(TASK_UPDATE_BY_INDEX));
        assertEquals(0,taskListener.notify(SHORT_TASK_ADD_TO_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(TASK_ADD_TO_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(TASK_REMOVE_FROM_PROJECT_BY_IDS));
        assertEquals(0,taskListener.notify(SHORT_TASK_LISTS_BY_PROJECT_ID));
        assertEquals(0,taskListener.notify(TASK_LISTS_BY_PROJECT_ID));
        assertEquals(0,taskListener.notify(SHORT_SAVE_TO_JSON));
        assertEquals(0,taskListener.notify(SAVE_TO_JSON));
        assertEquals(0,taskListener.notify(SHORT_SAVE_TO_XML));
        assertEquals(0,taskListener.notify(SAVE_TO_XML));
        assertEquals(0,taskListener.notify(SHORT_HELP));
        assertEquals(0,taskListener.notify(HELP));
        assertEquals(0,taskListener.notify(SHORT_LOAD_FROM_XML));
        assertEquals(0,taskListener.notify(LOAD_FROM_XML));
        assertEquals(0,taskListener.notify(SHORT_LOAD_FROM_JSON));
        assertEquals(0,taskListener.notify(LOAD_FROM_JSON));
        assertEquals(0,taskListener.notify(SHORT_USER_OF_TASK_SET_BY_INDEX));
        assertEquals(0,taskListener.notify(USER_OF_TASK_SET_BY_INDEX));
        assertDoesNotThrow(()-> {
            Mockito.doThrow(IOException.class).when(taskService).loadFrom(any(),any());
            taskListener.notify(LOAD_FROM_JSON);
        });


    }

    @Test
    void updateTaskByIndex() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
 
        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,user.getId(),true);
            taskListener.updateTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            assertEquals(0,taskListener.updateTaskByIndex());
        });
    }

    @Test
    void removeTaskByName() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();

        assertDoesNotThrow(()->{
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            taskListener.removeTaskByName();

            user.setRole(Role.ADMIN);
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            taskListener.removeTaskByName();
        });

    }

    @Test
    void removeTaskByID() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskByID();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskByID();
        });
    }

    @Test
    void removeTaskByIndex() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            taskListener.removeTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            taskListener.removeTaskByIndex();
        });
    }

    @Test
    void createTask() {
        doReturn(user).when(userListener).getUser();
        LocalDateTime localDateTime = LocalDateTime.now();
        assertDoesNotThrow(()-> {
            doReturn("tn").when(inputConsole).getNextInputString("task name",false);
            doReturn("td").when(inputConsole).getNextInputString("task description",false);
            doReturn(localDateTime).when(inputConsole).getNextInputLocalDateTime("deadline (default +8 hours)",false);
            assertEquals(0, taskListener.createTask());
            verify(taskService,times(1)).create("tn","td",taskListener.getUser().getId(),localDateTime);
        });
    }

    @Test
    void clearTask() {
        user.setRole(Role.USER);

        assertDoesNotThrow(()->{
            doReturn(null).when(userListener).getUser();
            taskListener.clearTask();

            doReturn(user).when(userListener).getUser();
            taskListener.clearTask();

            user.setRole(Role.ADMIN);
            taskListener.clearTask();
        });
    }

    @Test
    void viewTaskByIndex() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,user.getId(),true);
            taskListener.viewTaskByIndex();

            user.setRole(Role.ADMIN);
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            taskListener.viewTaskByIndex();
        });
    }

    @Test
    void listTask() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task());
        assertDoesNotThrow(()->{
            doReturn(tasks).when(taskService).findAll();
            taskListener.listTask();

            user.setRole(Role.ADMIN);
            taskListener.listTask();
        });

    }

    @Test
    void listTaskByProjectId() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task());
        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn(tasks).when(taskService).findAllByProjectId(1L);
            taskListener.listTaskByProjectId();

            user.setRole(Role.ADMIN);
            taskListener.listTaskByProjectId();
        });
    }

    @Test
    void addTaskToProjectByIds() {
        assertDoesNotThrow(()->{
        doReturn("1").when(inputConsole).getNextInputString("project ID",true);
        doReturn("1").when(inputConsole).getNextInputString("task ID",true);
        taskListener.addTaskToProjectByIds();
        verify(projectTaskService,times(1)).addTaskToProject(1L,1L);
        });
    }

    @Test
    void removeTaskFromProjectByIds() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            taskListener.removeTaskFromProjectByIds();

            doReturn("1").when(inputConsole).getNextInputString("project ID",true);
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            user.setRole(Role.ADMIN);
            taskListener.removeTaskFromProjectByIds();
        });
    }

    @Test
    void setTaskUserById() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
        user.setRole(Role.ADMIN);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);
        task.setName("tn");
        task.setDescription("td");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task ID",true);
            doReturn(task).when(taskService).findById(1L,true);
            taskListener.setTaskUserById(user);
        });
    }

    @Test
    void setTaskUserByIndexUser() {
        user.setRole(Role.USER);
        doReturn(user).when(userListener).getUser();
        user.setRole(Role.ADMIN);

        Task task = new Task();
        task.setUserId(user.getId());
        task.setId(0L);
        task.setName("tn");
        task.setDescription("td");

        assertDoesNotThrow(()->{
            doReturn("1").when(inputConsole).getNextInputString("task index",true);
            doReturn(task).when(taskService).findByIndex(0,true);
            taskListener.setTaskUserByIndex(user);
        });

    }

    @Test
    void setTaskUserByIndex() {
        user.setRole(Role.USER);
        user.setPasswordHash("ph");
        user.setFirstName("fn");
        user.setSecondName("sn");
        user.setMiddleName("mn");
        user.setId(0L);
        userListener.setUser(user);

        assertDoesNotThrow(() -> {
            assertEquals(0, taskListener.setTaskUserByIndex());

            user.setRole(Role.ADMIN);
            doReturn("login").when(inputConsole).getNextInputString("user login", false);
            doReturn(user).when(userListener).findByLogin("login");
            assertEquals(0, taskListener.setTaskUserByIndex());
        });
    }

}
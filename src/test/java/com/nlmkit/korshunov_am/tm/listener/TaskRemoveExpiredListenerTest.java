package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.TaskRemoveExpiredService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.USER_END;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class TaskRemoveExpiredListenerTest {
    static TaskRemoveExpiredListener taskRemoveExpiredListener;
    static TaskRemoveExpiredService taskRemoveExpiredService;
    static UserListener userListener;
    static InputConsole inputConsole;
    static ScheduledExecutorService executorService;
    static CommandHistoryService commandHistoryService;
    static ScheduledFuture scheduledFuture;


    @BeforeAll
    static void BeforeAll() {
        taskRemoveExpiredService = Mockito.mock(TaskRemoveExpiredService.class);
        userListener = Mockito.mock(UserListener.class);
        executorService = Mockito.mock(ScheduledExecutorService.class);
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        taskRemoveExpiredListener=new TaskRemoveExpiredListener(taskRemoveExpiredService,userListener,executorService,commandHistoryService);
        scheduledFuture=Mockito.mock(ScheduledFuture.class);
        taskRemoveExpiredListener.setConsole(inputConsole);
    }

    @Test
    void getUser() {
        User user = new User("login");
        doReturn(user).when(userListener).getUser();
        assertEquals(user,taskRemoveExpiredListener.getUser());
    }

    @Test
    void getInstance() {
        TaskRemoveExpiredListener taskRemoveExpiredListener = TaskRemoveExpiredListener.getInstance();
        assertEquals(taskRemoveExpiredListener,TaskRemoveExpiredListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,taskRemoveExpiredListener.notify("aaaaaa"));
        assertEquals(0,taskRemoveExpiredListener.notify(SHORT_USER_AUTH));
        assertEquals(0,taskRemoveExpiredListener.notify(USER_AUTH));
        assertEquals(0,taskRemoveExpiredListener.notify(SHORT_EXIT));
        assertEquals(0,taskRemoveExpiredListener.notify(EXIT));
        assertEquals(0,taskRemoveExpiredListener.notify(SHORT_USER_END));
        assertEquals(0,taskRemoveExpiredListener.notify(USER_END));
    }

    @Test
    void removeExpiredTasks() {
        assertDoesNotThrow(()->taskRemoveExpiredListener.removeExpiredTasks());
        User user = new User("login");
        user.setId(1L);
        doReturn(user).when(userListener).getUser();
        assertDoesNotThrow(()->taskRemoveExpiredListener.removeExpiredTasks());
        verify(taskRemoveExpiredService,times(1)).removeExpiredTasks(user.getId());
    }

    @Test
    void startRemoveExpiredTasksStopRemoveExpiredTasks() {
        doReturn(scheduledFuture).when(executorService).scheduleAtFixedRate(any(Runnable.class), anyLong(), anyLong(), any(TimeUnit.class));
        assertDoesNotThrow(()->taskRemoveExpiredListener.startRemoveExpiredTasks());
        assertDoesNotThrow(()->taskRemoveExpiredListener.startRemoveExpiredTasks());
        assertDoesNotThrow(()->taskRemoveExpiredListener.stopRemoveExpiredTasks());
        assertDoesNotThrow(()->taskRemoveExpiredListener.stopRemoveExpiredTasks());
    }
}
package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.publisher.InputConsole;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.TaskWarningService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;

class TaskWarningListenerTest {
    static TaskWarningListener taskWarningListener;
    static TaskWarningService taskWarningService;
    static UserListener userListener;
    static InputConsole inputConsole;
    static ScheduledExecutorService executorService;
    static CommandHistoryService commandHistoryService;
    static ScheduledFuture scheduledFuture;

    @BeforeAll
    static void BeforeAll() {
        taskWarningService = Mockito.mock(TaskWarningService.class);
        userListener = Mockito.mock(UserListener.class);
        executorService = Mockito.mock(ScheduledExecutorService.class);
        commandHistoryService = Mockito.mock(CommandHistoryService.class);
        inputConsole =  Mockito.mock(InputConsole.class);
        taskWarningListener=new TaskWarningListener(taskWarningService,userListener,executorService,commandHistoryService);
        scheduledFuture=Mockito.mock(ScheduledFuture.class);
        taskWarningListener.setConsole(inputConsole);
    }

    @Test
    void getUser() {
        User user = new User("login");
        doReturn(user).when(userListener).getUser();
        assertEquals(user,taskWarningListener.getUser());
    }

    @Test
    void getInstance() {
        TaskWarningListener taskWarningListener = TaskWarningListener.getInstance();
        assertEquals(taskWarningListener,TaskWarningListener.getInstance());
    }

    @Test
    void testNotify() {
        assertEquals(-1,taskWarningListener.notify("aaaaaa"));
        assertEquals(0,taskWarningListener.notify(SHORT_USER_AUTH));
        assertEquals(0,taskWarningListener.notify(USER_AUTH));
        assertEquals(0,taskWarningListener.notify(SHORT_EXIT));
        assertEquals(0,taskWarningListener.notify(EXIT));
        assertEquals(0,taskWarningListener.notify(SHORT_USER_END));
        assertEquals(0,taskWarningListener.notify(USER_END));
    }

    @Test
    void showWarnings() {
        Task task = new Task();
        task.setName("task");
        task.setDeadLine(LocalDateTime.now().plusMinutes(30).truncatedTo(ChronoUnit.SECONDS));
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        task.setId(1L);
        assertDoesNotThrow(()->taskWarningListener.ShowWarnings());
        User user = new User("login");
        user.setId(1L);
        doReturn(user).when(userListener).getUser();
        doReturn(tasks).when(taskWarningService).findTaskForWarning(user.getId());
        assertDoesNotThrow(()->taskWarningListener.ShowWarnings());
    }

    @Test
    void startShowWarningsStopShowWarnings() {
        doReturn(scheduledFuture).when(executorService).scheduleAtFixedRate(any(Runnable.class), anyLong(), anyLong(), any(TimeUnit.class));
        assertDoesNotThrow(()->taskWarningListener.startShowWarnings());
        assertDoesNotThrow(()->taskWarningListener.startShowWarnings());
        assertDoesNotThrow(()->taskWarningListener.stopShowWarnings());
        assertDoesNotThrow(()->taskWarningListener.stopShowWarnings());
    }

}
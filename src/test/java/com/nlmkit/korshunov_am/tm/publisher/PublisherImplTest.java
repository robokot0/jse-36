package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.listener.CommandHistoryListener;
import com.nlmkit.korshunov_am.tm.listener.Listener;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.nlmkit.korshunov_am.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

class PublisherImplTest {
    static PublisherImpl publisher;
    static InputConsole inputConsole;
    static Listener listener;
    static CommandHistoryListener commandHistoryListener;

    @BeforeAll
    public static void doBeforeAll() {
        inputConsole =  Mockito.mock(InputConsole.class);
        commandHistoryListener = Mockito.mock(CommandHistoryListener.class);
        listener = Mockito.mock(Listener.class);
        Publisher publisher0 = new PublisherImpl(inputConsole);
        publisher0.setConsole(inputConsole);
        publisher = new PublisherImpl(inputConsole,commandHistoryListener);
        publisher.addListener(listener);
        publisher.setConsole(inputConsole);
    }

    @Test
    void addListener() {
        assertDoesNotThrow(()->{
            publisher.addListener(listener);
            publisher.addListener(listener);
        });
    }

    @Test
    void deleteListener() {
        assertDoesNotThrow(()->{
            publisher.addListener(listener);
            publisher.deleteListener(listener);
            publisher.deleteListener(listener);
        });
    }

    @Test
    void notifyListener() {
        assertThrows(WrongArgumentException.class,()->{
            doReturn(-1).when(listener).notify("1111");
            publisher.notifyListener("1111");
        });
        assertDoesNotThrow(()->{
            publisher.addListener(listener);
            doReturn(0).when(listener).notify("command");
            publisher.notifyListener("command");
        });
    }

    @Test
    void readAndExecuteCommand() {
        publisher.setConsole(inputConsole);

        assertDoesNotThrow(()->{
            doReturn(EXIT).when(inputConsole).getNextInputString("command",true);
            publisher.readAndExecuteCommand();

            doReturn(SHORT_EXIT).when(inputConsole).getNextInputString("command",true);
            publisher.readAndExecuteCommand();

            doReturn("TEST").doReturn(EXIT).when(inputConsole).getNextInputString("command",true);
            doReturn(-1).when(listener).notify("TEST");
            publisher.readAndExecuteCommand();


        });

    }

}
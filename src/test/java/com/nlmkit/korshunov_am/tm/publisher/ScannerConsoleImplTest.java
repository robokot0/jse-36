package com.nlmkit.korshunov_am.tm.publisher;

import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;
import static org.junit.jupiter.api.Assertions.*;

class ScannerConsoleImplTest {
    static ScannerConsoleImpl scannerConsole;

    @BeforeAll
    static void doBeforeAll() {
        scannerConsole = ScannerConsoleImpl.getInstance();
    }

    @Test
    void getInstance() {
        ScannerConsoleImpl scannerConsole = ScannerConsoleImpl.getInstance();
        assertEquals(scannerConsole,ScannerConsoleImpl.getInstance());
    }

    @Test
    void getNextInputString() {
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputString("pn",false));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputString("pn",true));
        });
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("pp\n"));
            assertEquals("pp",scannerConsole.getNextInputString("pn",true));
        });
    }

    @Test
    void getNextInputInteger() {
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputInteger("pn",false));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputInteger("pn",true));
        });
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("1\n"));
            assertEquals(1,scannerConsole.getNextInputInteger("pn",true));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("aa\n"));
            scannerConsole.getNextInputInteger("pn",true);
        });
    }

    @Test
    void getNextInputLong() {
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputLong("pn",false));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputLong("pn",true));
        });
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("1\n"));
            assertEquals(1L,scannerConsole.getNextInputLong("pn",true));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("aa\n"));
            scannerConsole.getNextInputLong("pn",true);
        });
    }

    @Test
    void getNextInputPassword() {
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputPassword("pn",false));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputPassword("pn",true));
        });
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("pp\n"));
            assertEquals("pp",scannerConsole.getNextInputPassword("pn",true));
        });
    }
    @Test
    void getNextInputLocalDateTime() {
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputLocalDateTime("pn",false));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("\n"));
            assertNull(scannerConsole.getNextInputLocalDateTime("pn",true));
        });
        assertThrows(WrongArgumentException.class,()->{
            scannerConsole.setScanner(new Scanner("aaa\n"));
            assertNull(scannerConsole.getNextInputLocalDateTime("pn",true));
        });
        assertDoesNotThrow(()->{
            scannerConsole.setScanner(new Scanner("1\n"));
            LocalDateTime localDateTime1 = LocalDateTime.now().plusHours(1);
            LocalDateTime localDateTime2 = scannerConsole.getNextInputLocalDateTime("pn",true);
            localDateTime1.until(localDateTime2, ChronoUnit.SECONDS);
            assertEquals(localDateTime1.until(localDateTime2, ChronoUnit.SECONDS),0);
        });
    }
    @Test
    void showMessage() {
        assertDoesNotThrow(()-> scannerConsole.showMessage("message"));
    }
}
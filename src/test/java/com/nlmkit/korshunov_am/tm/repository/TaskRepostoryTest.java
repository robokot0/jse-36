package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TaskRepostoryTest {
    static TaskRepostory taskRepostory = TaskRepostory.getInstance();
    @BeforeAll
    public static void setUp() throws Exception {

        taskRepostory.create("P2","D1",1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("P1","D2",1L, LocalDateTime.now().plusHours(15));

        ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
        taskRepostory.saveAs(new JsonMapper(),byteArrayOutputStream1);
        taskRepostory.clear();
        ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream(byteArrayOutputStream1.toByteArray());
        taskRepostory.loadFrom(new JsonMapper(),byteArrayInputStream1);

        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        taskRepostory.saveAs(new XmlMapper(),byteArrayOutputStream2);
        taskRepostory.clear();
        ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(byteArrayOutputStream2.toByteArray());
        taskRepostory.loadFrom(new XmlMapper(),byteArrayInputStream2);
        taskRepostory.clear();
    }

    @Test
    public void createByNameUserId() {
        Task task1 = taskRepostory.create("aaa", Long.MAX_VALUE);
        assertEquals(task1.getId().longValue(), taskRepostory.findById(task1.getId()).getId().longValue());
        assertEquals(task1.getName(), taskRepostory.findById(task1.getId()).getName());
    }
    @Test
    public void createByNameDescriptionUserId() {
        Task task2 = taskRepostory.create("aaa", "bbb", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(task2.getId().longValue(), taskRepostory.findById(task2.getId()).getId().longValue());
        assertEquals(taskRepostory.findById(task2.getId()).getName(), "aaa");
        assertEquals(taskRepostory.findById(task2.getId()).getDescription(), "bbb");
    }
    @Test
    public void update() {
        Task task = taskRepostory.create("aaa1", "bbb1", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.update(task.getId(), "aaa2", "bbb2", Long.MAX_VALUE);
        assertEquals(taskRepostory.findById(task.getId()).getName(), "aaa2");
        assertEquals(taskRepostory.findById(task.getId()).getDescription(), "bbb2");
    }

    @Test
    public void updatedeadline() {
        Task task = taskRepostory.create("aaa1", "bbb1", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.update(task.getId(), "aaa2", "bbb2", Long.MAX_VALUE,null);
        assertNull(taskRepostory.findById(task.getId()).getDeadLine());
    }

    @Test
    public void testClear() {
        taskRepostory.create("aaa", Long.MAX_VALUE);
        taskRepostory.create("aaa", Long.MIN_VALUE);
        assertNotEquals(taskRepostory.size(Long.MAX_VALUE), 0);
        assertNotEquals(taskRepostory.size(Long.MIN_VALUE), 0);
        taskRepostory.clear(Long.MAX_VALUE);
        assertEquals(taskRepostory.size(Long.MAX_VALUE), 0);
        assertNotEquals(taskRepostory.size(Long.MIN_VALUE), 0);
    }

    @Test
    public void findByIndex() {
        taskRepostory.clear();
        taskRepostory.create("aaa02", Long.MAX_VALUE);
        taskRepostory.create("aaa01", Long.MIN_VALUE);
        assertEquals(taskRepostory.findByIndex(0).getName(), "aaa01");
    }
    @Test
    public void findByIndexNotFound() {
        taskRepostory.clear();
        taskRepostory.create("aaa02", Long.MAX_VALUE);
        taskRepostory.create("aaa01", Long.MIN_VALUE);
        assertNull(taskRepostory.findByIndex(155));
    }

    @Test
    public void testFindByIndex() {
        taskRepostory.clear();
        taskRepostory.create("aaa003", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa000", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findByIndex(0, Long.MAX_VALUE).getName(), "aaa000");
        assertEquals(taskRepostory.findByIndex(0, Long.MIN_VALUE).getName(), "aaa003");
    }
    @Test
    public void testFindByIndexNull() {
        taskRepostory.clear();
        taskRepostory.create("aaa003", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa000", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertNull(taskRepostory.findByIndex(9999, Long.MAX_VALUE));
        assertNull(taskRepostory.findByIndex(88880, Long.MIN_VALUE));
    }

    @Test
    public void findByName() {
        taskRepostory.create("aaa3", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findByName("aaa3").getName(), "aaa3");
    }
    @Test
    public void findByNameNull() {
        assertNull(taskRepostory.findByName("aaa54654563"));
    }

    @Test
    public void testFindByName() {
        taskRepostory.create("aaa3", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findByName("aaa3", Long.MIN_VALUE).getName(), "aaa3");
        assertEquals(taskRepostory.findByName("aaa0", Long.MAX_VALUE).getName(), "aaa0");
    }
    @Test
    public void testFindByNameNull() {
        assertNull(taskRepostory.findByName("aaa3464655", Long.MIN_VALUE));
        assertNull(taskRepostory.findByName("aaa46546540", Long.MAX_VALUE));
    }

    @Test
    public void findById() {
        TaskRepostory taskRepostory = TaskRepostory.getInstance();
        Task task = taskRepostory.create("aaa3","bbb3",Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findById(task.getId()).getId(),task.getId());
    }

    @Test
    public void findByIdNull() {
        assertNull(taskRepostory.findById(6546546L));
    }

    @Test
    public void testFindById() {
        Task task = taskRepostory.create("aaa3", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        Task task1 = taskRepostory.create("aaa0", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findById(task.getId(),Long.MIN_VALUE).getId(),task.getId());
        assertEquals(taskRepostory.findById(task1.getId(),Long.MAX_VALUE).getId(),task1.getId());
    }

    @Test
    public void testFindByIdNull() {
        taskRepostory.create("aaa3", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", Long.MAX_VALUE, LocalDateTime.now().plusHours(15));
        assertNull(taskRepostory.findById(4465456456L,Long.MIN_VALUE));
        assertNull(taskRepostory.findById(4464645185L,Long.MAX_VALUE));
    }

    @Test
    public void removeByIndex() {
        taskRepostory.create("333333333aaa3", "bbb3", 55L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("000000aaa0", "bbb3", 55L, LocalDateTime.now().plusHours(15));
        taskRepostory.removeByIndex(0);
        assertEquals(taskRepostory.findByIndex(0).getName(),"333333333aaa3");
    }

    @Test
    public void testRemoveByIndex() {
        taskRepostory.create("aaa3", "bbb3", 11L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", 11L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa3", "bbb3", 21L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", 21L, LocalDateTime.now().plusHours(15));
        taskRepostory.removeByIndex(0,11L);
        assertEquals(taskRepostory.findByIndex(0,11L).getName(),"aaa3");
        assertEquals(taskRepostory.findByIndex(0,21L).getName(),"aaa0");
    }

    @Test
    public void removeById() {
        taskRepostory.clear();
        Task task = taskRepostory.create("aaa3", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertNotEquals(taskRepostory.size(),0);
        taskRepostory.removeById(task.getId());
        assertEquals(taskRepostory.size(),0);
    }

    @Test
    public void testRemoveById() {
        taskRepostory.clear();
        Task task1 = taskRepostory.create("aaa3", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa3", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa0", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.size(1L),2);
        assertEquals(taskRepostory.size(2L),2);
        taskRepostory.removeById(task1.getId(),1L);
        assertEquals(taskRepostory.size(1L),1);
        assertEquals(taskRepostory.size(2L),2);
    }

    @Test
    public void removeByName() {
        taskRepostory.clear();
        Task task1 = taskRepostory.create("aaa1", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.size(),2);
        taskRepostory.removeByName(task1.getName());
        assertEquals(taskRepostory.size(),1);
    }

    @Test
    public void testRemoveByName() {
        taskRepostory.clear();
        Task task1 = taskRepostory.create("aaa1", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa3", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa4", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.size(1L),2);
        assertEquals(taskRepostory.size(2L),2);
        taskRepostory.removeByName(task1.getName(),1L);
        assertEquals(taskRepostory.size(1L),1);
        assertEquals(taskRepostory.size(2L),2);
    }

    @Test
    public void findAll() {
        taskRepostory.clear();
        taskRepostory.create("aaa1", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findAll().size(),2);
    }

    @Test
    public void testFindAll() {
        taskRepostory.clear();
        taskRepostory.create("aaa1", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa3", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.findAll(1L).size(),2);
        assertEquals(taskRepostory.findAll(2L).size(),1);
    }

    @Test
    public void size() {
        taskRepostory.clear();
        taskRepostory.create("aaa1", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", Long.MIN_VALUE, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.size(),2);
    }

    @Test
    public void testSize() {
        taskRepostory.clear();
        taskRepostory.create("aaa1", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa2", "bbb3", 1L, LocalDateTime.now().plusHours(15));
        taskRepostory.create("aaa3", "bbb3", 2L, LocalDateTime.now().plusHours(15));
        assertEquals(taskRepostory.size(),3);
    }

    @Test
    public void getInstance() {
        assertEquals(taskRepostory,TaskRepostory.getInstance());
    }


    @Test
    public void findByProjectIdAndId() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("findByProjectIdAndId1",1L);
        Task task = taskRepostory.create("findByProjectIdAndId1",1L);

        assertNull(taskRepostory.findByProjectIdAndId(project.getId(),11L));
        assertNull(taskRepostory.findByProjectIdAndId(project.getId(),task.getId()));
        task.setProjectId(project.getId());
        assertEquals(task,taskRepostory.findByProjectIdAndId(project.getId(),task.getId()));
        task.setProjectId(0L);
        assertNull(taskRepostory.findByProjectIdAndId(project.getId(),task.getId()));
    }

    @Test
    public void findByProjectIdAndIdNull() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("findByProjectIdAndId1",1L);
        Task task1 = taskRepostory.create("findByProjectIdAndId1",1L);
        assertNull(taskRepostory.findByProjectIdAndId(task1.getId(),project.getId()));
    }


    @Test
    public void testFindByProjectIdAndId() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("testFindByTaskIdAndId1",1L);
        Task task1 = taskRepostory.create("testFindByTaskIdAndId1",1L);
        taskRepostory.findAddByProjectId(project.getId(),task1.getId(),1L);
        assertEquals(task1,taskRepostory.findByProjectIdAndId(project.getId(),task1.getId(),1L));
    }

    @Test
    public void testFindByTaskIdAndIdNull() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("testFindByTaskIdAndId1",1L);
        Task task1 = taskRepostory.create("testFindByTaskIdAndId1",1L);
        assertNull(taskRepostory.findByProjectIdAndId(task1.getId(),project.getId(),1L));
    }

    @Test
    public void testFindByProjectIdAndIdNullUserId() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("testFindByTaskIdAndId1",1L);
        Task task1 = taskRepostory.create("testFindByTaskIdAndId1",1L);
        taskRepostory.findAddByProjectId(project.getId(),task1.getId(),1L);
        assertNull(taskRepostory.findByProjectIdAndId(project.getId(),task1.getId(),2L));
    }

    @Test
    public void findAllByProjectId() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("findAllByProjectId1",1L);
        Task task1 = taskRepostory.create("findAllByProjectId1",1L);
        taskRepostory.findAddByProjectId(project.getId(),task1.getId());
        assertEquals(taskRepostory.findAllByProjectId(project.getId()).size(),1);
    }

    @Test
    public void findAllByProjectIdNull() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("findAllByProjectIdNull1",1L);
        assertEquals(taskRepostory.findAllByProjectId(project.getId()).size(),0);
    }

    @Test
    public void testFindAllByProjectId() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("testFindAllByProjectId1",1L);
        Task task1 = taskRepostory.create("testFindAllByProjectId1",1L);
        taskRepostory.findAddByProjectId(project.getId(),task1.getId(),1L);
        assertEquals(taskRepostory.findAllByProjectId(project.getId(),1L).size(),1);
    }

    @Test
    public void testFindAllByProjectIdNull() {
        ProjectRepository projectRepository = ProjectRepository.getInstance();
        Project project = projectRepository.create("testFindAllByProjectIdNull1",1L);
        assertEquals(taskRepostory.findAllByProjectId(project.getId(),1L).size(),0);
    }

    @Test
    public void getEntityClass() {
        assertEquals(taskRepostory.getEntityClass(),Task.class);
    }

    @Test
    public void  findAllByDeadLinePeriod() {

        LocalDateTime localDateTime1 = LocalDateTime.now().plusHours(4501);
        LocalDateTime localDateTime2 = LocalDateTime.now().plusHours(4502);

        taskRepostory.create("TN0000","TD0000",1111L,null);
        taskRepostory.create("TN1111","TD1111",1111L,localDateTime1);
        taskRepostory.create("TN2222","TD2222",1111L,localDateTime2);
        taskRepostory.create("TN3333","TD3333",3333L,localDateTime1);

        List<Task> taskList = taskRepostory.findAllByDeadLinePeriod(localDateTime1.minusMinutes(30),localDateTime1.plusMinutes(30),1111L);

        assertEquals(1,taskList.size());
    }


}
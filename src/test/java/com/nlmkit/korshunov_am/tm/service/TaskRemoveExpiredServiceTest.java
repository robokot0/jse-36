package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TaskRemoveExpiredServiceTest {

    @Test
    void getInstance() {
        TaskRemoveExpiredService taskRemoveExpiredService = TaskRemoveExpiredService.getInstance();
        assertEquals(taskRemoveExpiredService,TaskRemoveExpiredService.getInstance());
    }

    @Test
    void removeExpiredTasks() {
        TaskRepostory taskRepostory = Mockito.mock(TaskRepostory.class);
        TaskRemoveExpiredService taskRemoveExpiredService = new TaskRemoveExpiredService(taskRepostory);

        LocalDateTime localDateTime4 = LocalDateTime.now().minusSeconds(30);
        Task task = new Task();
        task.setId(1L);
        task.setDeadLine(localDateTime4);
        task.setUserId(1L);
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        Mockito.doReturn(taskList).when(taskRepostory).findAllByDeadLinePeriod(eq(LocalDateTime.MIN),any(LocalDateTime.class),eq(1L));
        assertDoesNotThrow(()->taskRemoveExpiredService.removeExpiredTasks(1L));
        verify(taskRepostory,times(1)).findAllByDeadLinePeriod(eq(LocalDateTime.MIN),any(LocalDateTime.class),eq(1L));
        verify(taskRepostory,times(1)).removeById(task.getId());
    }
}
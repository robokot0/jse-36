package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class TaskWarningServiceTest {

    @Test
    void getInstance() {
        TaskWarningService taskWarningService = TaskWarningService.getInstance();
        assertEquals(taskWarningService,TaskWarningService.getInstance());
    }

    @Test
    void findTaskForWarning() {
        TaskRepostory taskRepostory = Mockito.mock(TaskRepostory.class);
        TaskWarningService taskWarningService = new TaskWarningService(taskRepostory);
        LocalDateTime localDateTime4 = LocalDateTime.now().plusHours(4).truncatedTo(ChronoUnit.SECONDS);
        Task task = new Task();
        task.setDeadLine(localDateTime4);
        task.setUserId(1L);
        Task task2 = new Task();
        task2.setDeadLine(localDateTime4.plusMinutes(5));
        task2.setUserId(1L);
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        taskList.add(task2);
        Mockito.doReturn(taskList).when(taskRepostory).findAllByDeadLinePeriod(any(),any(),any());
        assertEquals(1,taskWarningService.findTaskForWarning(1L).size());
        assertEquals(task.getId(),taskWarningService.findTaskForWarning(1L).get(0).getId());
        taskList.clear();
        assertEquals(0,taskWarningService.findTaskForWarning(1L).size());
    }
}